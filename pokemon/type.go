package pokemon

type Type struct {
	Name            string `json:"name"`
	Generation      Form   `json:"generation"`
	DamageRelations `json:"damage_relations"`
	GameIndices     []GameIndexType `json:"game_indices"`
	MoveDamageClass Form            `json:"move_damage_class"`
	Moves           []Form          `json:"moves"`
	Pokemon         []Pkm           `json:"pokemon"`
	ID              int             `json:"id"`
	Names           []Name          `json:"names"`
}

type GameIndexType struct {
	Generation Form `json:"generation"`
	GameIndex  int  `json:"game_index"`
}

type Pkm struct {
	Slot    int  `json:"slot"`
	Pokemon Form `json:"pokemon"`
}

type DamageRelations struct {
	HalfDamageFrom   []Form        `json:"half_damage_from"`
	NoDamageFrom     []interface{} `json:"no_damage_from"`
	HalfDamageTo     []Form        `json:"half_damage_to"`
	DoubleDamageFrom []Form        `json:"double_damage_from"`
	NoDamageTo       []Form        `json:"no_damage_to"`
	DoubleDamageTo   []Form        `json:"double_damage_to"`
}
