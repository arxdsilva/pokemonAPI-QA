package pokemon

type Pokemons struct {
	Count    int         `json:"count"`
	Previous interface{} `json:"previous"`
	Results  []Form      `json:"results"`
	Next     string      `json:"next"`
}
