package pokemon

type Pokemon struct {
	Abilities              []Ability   `json:"abilities"`
	BaseExperience         int64       `json:"base_experience"`
	Forms                  []Form      `json:"forms"`
	GameIndices            []GameIndex `json:"game_indices"`
	Height                 int64       `json:"height"`
	HeldItems              []HeldItem  `json:"held_items"`
	ID                     int64       `json:"id"`
	IsDefault              bool        `json:"is_default"`
	LocationAreaEncounters string      `json:"location_area_encounters"`
	Moves                  []Move      `json:"moves"`
	Name                   string      `json:"name"`
	Order                  int64       `json:"order"`
	Species                Form        `json:"species"`
	Sprites                Sprite      `json:"sprites"`
	Stats                  []Stat      `json:"stats"`
	Types                  []OneType   `json:"types"`
	Weight                 int64       `json:"weight"`
}

type Ability struct {
	Ability  Form  `json:"ability"`
	IsHidden bool  `json:"is_hidden"`
	Slot     int64 `json:"slot"`
}

type Form struct {
	Name string `json:"name"`
	URL  string `json:"url"`
}

type Stat struct {
	BaseStat int64 `json:"base_stat"`
	Effort   int64 `json:"effort"`
	Stat     Form  `json:"stat"`
}

type OneType struct {
	Slot int64 `json:"slot"`
	Type Form  `json:"type"`
}

type Sprite struct {
	BackDefault      string `json:"back_default"`
	BackFemale       string `json:"back_female"`
	BackShiny        string `json:"back_shiny"`
	BackShinyFemale  string `json:"back_shiny_female"`
	FrontDefault     string `json:"front_default"`
	FrontFemale      string `json:"front_female"`
	FrontShiny       string `json:"front_shiny"`
	FrontShinyFemale string `json:"front_shiny_female"`
}

type VersionGroupDetail struct {
	LevelLearnedAt  int64 `json:"level_learned_at"`
	MoveLearnMethod Form  `json:"move_learn_method"`
	VersionGroup    Form  `json:"version_group"`
}

type Move struct {
	Move                Form                 `json:"move"`
	VersionGroupDetails []VersionGroupDetail `json:"version_group_details"`
}

type VersionDetail struct {
	Rarity  int64 `json:"rarity"`
	Version Form  `json:"version"`
}

type HeldItem struct {
	Item           Form            `json:"item"`
	VersionDetails []VersionDetail `json:"version_details"`
}

type GameIndex struct {
	GameIndex int64 `json:"game_index"`
	Version   Form  `json:"version"`
}
