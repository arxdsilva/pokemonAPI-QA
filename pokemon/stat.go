package pokemon

type StatEndp struct {
	IsBattleOnly     bool   `json:"is_battle_only"`
	Names            []Name `json:"names"`
	AffectingNatures `json:"affecting_natures"`
	Characteristics  []Characteristic `json:"characteristics"`
	AffectingMoves   `json:"affecting_moves"`
	MoveDamageClass  interface{} `json:"move_damage_class"`
	GameIndex        int         `json:"game_index"`
	ID               int         `json:"id"`
	Name             string      `json:"name"`
}

type Characteristic struct {
	URL string `json:"url"`
}

type AffectingNatures struct {
	Increase []interface{} `json:"increase"`
	Decrease []interface{} `json:"decrease"`
}

type AffectingMoves struct {
	Increase []Increaser   `json:"increase"`
	Decrease []interface{} `json:"decrease"`
}

type Increaser struct {
	Move   Form `json:"move"`
	Change int  `json:"change"`
}

type Name struct {
	Name     string `json:"name"`
	Language Form   `json:"language"`
}
