package pokemon

type Pokedex struct {
	Count    int         `json:"count"`
	Previous interface{} `json:"previous"`
	Results  []Form      `json:"results"`
	Next     interface{} `json:"next"`
}
