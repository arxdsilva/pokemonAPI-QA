package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/arxdsilva/pokemonAPI-QA/pokemon"
	. "gopkg.in/check.v1"
)

// Pokemon Endpoint
// 1. The pokemon endpoint should accept both, the pokemon name and the pokemon id as
// parameter, and the response should be exactly the same for both calls.
func (s *S) TestEndpointAcceptsNameAndPkmNumber(c *C) {
	var pkmnNumber = new(pokemon.Pokemon)
	resp, err := http.Get("https://pokeapi.co/api/v2/pokemon/1")
	c.Assert(err, IsNil)
	defer resp.Body.Close()
	c.Assert(resp, NotNil)
	c.Assert(resp.StatusCode, Equals, http.StatusOK)
	body, err := ioutil.ReadAll(resp.Body)
	c.Assert(err, IsNil)
	err = json.Unmarshal(body, &pkmnNumber)
	c.Assert(err, IsNil)
	var pkmnName = new(pokemon.Pokemon)
	respNum, err := http.Get("https://pokeapi.co/api/v2/pokemon/bulbasaur")
	c.Assert(err, IsNil)
	defer respNum.Body.Close()
	c.Assert(respNum, NotNil)
	body, err = ioutil.ReadAll(respNum.Body)
	c.Assert(err, IsNil)
	err = json.Unmarshal(body, &pkmnName)
	c.Assert(err, IsNil)
	c.Assert(pkmnName, DeepEquals, pkmnName)
}

// 2. The pokemon count on the pokemon endpoint should be 802 pokemons, and it should be
// paginated, with a link to the next page.
func (s *S) TestCountInPokemonEndpoint(c *C) {
	var endpoint = new(pokemon.Pokemons)
	resp, err := http.Get("https://pokeapi.co/api/v2/pokemon")
	c.Assert(err, IsNil)
	defer resp.Body.Close()
	c.Assert(resp, NotNil)
	c.Assert(resp.StatusCode, Equals, http.StatusOK)
	body, err := ioutil.ReadAll(resp.Body)
	c.Assert(err, IsNil)
	err = json.Unmarshal(body, &endpoint)
	c.Assert(err, IsNil)
	c.Assert(endpoint.Count, Equals, 802)
	c.Assert(endpoint.Next, NotNil)
}

// Pikachu
// 1. Pikachu should have an stat speed, and its base value should be 80.
func (s *S) TestPikachuStatSpeedAndBaseValue(c *C) {
	var pikachu = new(pokemon.Pokemon)
	resp, err := http.Get("https://pokeapi.co/api/v2/pokemon/pikachu")
	c.Assert(err, IsNil)
	defer resp.Body.Close()
	c.Assert(resp, NotNil)
	c.Assert(resp.StatusCode, Equals, http.StatusOK)
	body, err := ioutil.ReadAll(resp.Body)
	c.Assert(err, IsNil)
	err = json.Unmarshal(body, &pikachu)
	c.Assert(err, IsNil)
	var pikachuHasSpeedStat bool
	var pikachuSpeedValue int64
	for _, stat := range pikachu.Stats {
		if stat.Stat.Name == "speed" {
			pikachuHasSpeedStat = true
			pikachuSpeedValue = stat.BaseStat
		}
	}
	c.Assert(pikachuHasSpeedStat, Equals, true)
	c.Assert(pikachuSpeedValue, Equals, 80)
}

// 2. Pikachu should have Lightning Rod and Static abilities
func (s *S) TestPikachuShouldHaveLightningRodAndStaticAbilities(c *C) {
	var pikachu = new(pokemon.Pokemon)
	resp, err := http.Get("https://pokeapi.co/api/v2/pokemon/pikachu")
	c.Assert(err, IsNil)
	defer resp.Body.Close()
	c.Assert(resp, NotNil)
	c.Assert(resp.StatusCode, Equals, http.StatusOK)
	body, err := ioutil.ReadAll(resp.Body)
	c.Assert(err, IsNil)
	err = json.Unmarshal(body, &pikachu)
	c.Assert(err, IsNil)
	var pikachuHasStatic bool
	var pikachuHasLightningRod bool
	for _, ability := range pikachu.Abilities {
		if ability.Ability.Name == "static" {
			pikachuHasStatic = true
		}
		if ability.Ability.Name == "lightning-rod" {
			pikachuHasLightningRod = true
		}
	}
	c.Assert(pikachuHasStatic, Equals, true)
	c.Assert(pikachuHasLightningRod, Equals, true)
}

// 3. speed should have an truthy value for is_battle_only. (/stat endpoint)
func (s *S) TestPikachuSpeedShouldBeTrueForIsBattleOnly(c *C) {
	var pikachu = new(pokemon.Pokemon)
	resp, err := http.Get("https://pokeapi.co/api/v2/pokemon/pikachu")
	c.Assert(err, IsNil)
	defer resp.Body.Close()
	c.Assert(resp, NotNil)
	c.Assert(resp.StatusCode, Equals, http.StatusOK)
	body, err := ioutil.ReadAll(resp.Body)
	c.Assert(err, IsNil)
	err = json.Unmarshal(body, &pikachu)
	c.Assert(err, IsNil)
	var pikachuStatURL string
	var pikachuStatName string
	for _, stat := range pikachu.Stats {
		if stat.Stat.Name == "speed" {
			pikachuStatURL = stat.Stat.URL
			pikachuStatName = stat.Stat.Name
		}
	}
	pikachuQueryParameter := fmt.Sprintf("?name=%s", pikachuStatName)
	pikachuSpeedStatURL := pikachuStatURL + pikachuQueryParameter
	respStat, err := http.Get(pikachuSpeedStatURL)
	c.Assert(err, IsNil)
	defer respStat.Body.Close()
	c.Assert(respStat, NotNil)
	c.Assert(respStat.StatusCode, Equals, http.StatusOK)
	var pikachuStat = new(pokemon.StatEndp)
	bodyStat, err := ioutil.ReadAll(respStat.Body)
	c.Assert(err, IsNil)
	err = json.Unmarshal(bodyStat, &pikachuStat)
	c.Assert(err, IsNil)
	c.Assert(pikachuStat.IsBattleOnly, Equals, true)
}

// 4. Based on Pikachu’s type:
// 		❍ It should half damage from flying, steel, and eletric types
// 		❍ It should do double damage on flying and water types
// 		❍ It should take double damage from ground type
// 		❍ It shouldn't be immune to any type
func (s *S) TestPikachuType(c *C) {
	var pikachu = new(pokemon.Pokemon)
	resp, err := http.Get("https://pokeapi.co/api/v2/pokemon/pikachu")
	c.Assert(err, IsNil)
	defer resp.Body.Close()
	c.Assert(resp, NotNil)
	c.Assert(resp.StatusCode, Equals, http.StatusOK)
	body, err := ioutil.ReadAll(resp.Body)
	c.Assert(err, IsNil)
	err = json.Unmarshal(body, &pikachu)
	c.Assert(err, IsNil)
	pikachuType := pikachu.Types[0].Type.Name
	typeURL := fmt.Sprintf("https://pokeapi.co/api/v2/type/%s", pikachuType)
	var typeDecoded = new(pokemon.Type)
	respType, err := http.Get(typeURL)
	c.Assert(err, IsNil)
	defer respType.Body.Close()
	c.Assert(respType, NotNil)
	c.Assert(respType.StatusCode, Equals, http.StatusOK)
	bodyType, err := ioutil.ReadAll(respType.Body)
	c.Assert(err, IsNil)
	err = json.Unmarshal(bodyType, &typeDecoded)
	c.Assert(err, IsNil)
	halfDmgFr := make(map[string]struct{})
	for _, halfDmgFrom := range typeDecoded.HalfDamageFrom {
		if (halfDmgFrom.Name == "flying") || (halfDmgFrom.Name == "steel") || (halfDmgFrom.Name == "electric") {
			halfDmgFr[halfDmgFrom.Name] = struct{}{}
		}
	}
	c.Assert(len(halfDmgFr), Equals, 3)
	_, ok := halfDmgFr["flying"]
	c.Assert(ok, Equals, true)
	_, ok = halfDmgFr["steel"]
	c.Assert(ok, Equals, true)
	_, ok = halfDmgFr["electric"]
	c.Assert(ok, Equals, true)
	doubleDmgOn := make(map[string]struct{})
	for _, doubleDmg := range typeDecoded.DoubleDamageTo {
		if (doubleDmg.Name == "flying") || (doubleDmg.Name == "water") {
			doubleDmgOn[doubleDmg.Name] = struct{}{}
		}
	}
	c.Assert(len(halfDmgFr), Equals, 2)
	_, ok = halfDmgFr["flying"]
	c.Assert(ok, Equals, true)
	_, ok = halfDmgFr["water"]
	c.Assert(ok, Equals, true)
	c.Assert(typeDecoded.DoubleDamageFrom[0].Name, Equals, "ground")
	c.Assert(len(typeDecoded.NoDamageFrom), Equals, 0)
}

// Pokedex Endpoint
// All calls to pokedex should be authenticated.
// It's required to use the following JWT token:
// eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiQXNoIENhdGNodW0iLCJhZG1pbiI6dHJ1ZX0.O6aUYy5tRdUoBZiQ7UfjNdaIsBL0QW8EEOy2BkJQRyU

// 1. The pokedex endpoint should return an 401 status code if no authentication is used.
func (s *S) TestEndpointShouldFailWithoutToken(c *C) {
	resp, err := http.Get("https://pokeapi.co/api/v2/pokedex/1")
	c.Assert(err, IsNil)
	defer resp.Body.Close()
	c.Assert(resp, NotNil)
	c.Assert(resp.StatusCode, Equals, http.StatusUnauthorized)
}

// 2. The pokedex count on the pokedex endpoint should be 14.
func (s *S) TestPokedexCountShouldBe14(c *C) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", "https://pokeapi.co/api/v2/pokedex", nil)
	c.Assert(err, IsNil)
	req.Header.Set("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiQXNoIENhdGNodW0iLCJhZG1pbiI6dHJ1ZX0.O6aUYy5tRdUoBZiQ7UfjNdaIsBL0QW8EEOy2BkJQRyU")
	resp, err := client.Do(req)
	c.Assert(err, IsNil)
	defer resp.Body.Close()
	c.Assert(resp, NotNil)
	c.Assert(resp.StatusCode, Equals, http.StatusOK)
	body, err := ioutil.ReadAll(resp.Body)
	c.Assert(err, IsNil)
	pokedex := new(pokemon.Pokedex)
	err = json.Unmarshal(body, &pokedex)
	c.Assert(err, IsNil)
	c.Assert(pokedex.Count, Equals, 14)
}

// 3. There are three required items on the pokedex list:
// 		1. national
// 		2. kanto
// 		3. johto
func (s *S) TestPokedexRequiredItemsExists(c *C) {
	client := &http.Client{}
	req, err := http.NewRequest("GET", "https://pokeapi.co/api/v2/pokedex", nil)
	c.Assert(err, IsNil)
	req.Header.Set("Authorization", "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiQXNoIENhdGNodW0iLCJhZG1pbiI6dHJ1ZX0.O6aUYy5tRdUoBZiQ7UfjNdaIsBL0QW8EEOy2BkJQRyU")
	resp, err := client.Do(req)
	c.Assert(err, IsNil)
	defer resp.Body.Close()
	c.Assert(resp, NotNil)
	c.Assert(resp.StatusCode, Equals, http.StatusOK)
	body, err := ioutil.ReadAll(resp.Body)
	c.Assert(err, IsNil)
	pokedex := new(pokemon.Pokedex)
	err = json.Unmarshal(body, &pokedex)
	c.Assert(err, IsNil)
	pokedexWanted := make(map[string]struct{})
	for _, pkedex := range pokedex.Results {
		if (pkedex.Name == "national") || (pkedex.Name == "kanto") || (pkedex.Name == "johto") {
			pokedexWanted[pkedex.Name] = struct{}{}
		}
	}
	c.Assert(len(pokedexWanted), Equals, 3)
}
