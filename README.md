# pokemonAPI-QA

This is a simple QA test on pokemon API, to run it just do:

```go
    $ go get gitlab.com/arxdsilva/pokemonAPI-QA
    $ go test -v . // inside pokemonAPI-QA's repo
```

 Security disclaimer: It's best to use the JWT as a environment variable in any app, since this is a important auth key, instead of hardcoded as It's here.